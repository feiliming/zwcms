修改源码说明：
1.修改pom.xml，只修改Java和Tomcat版本，其他信息不用修改，打包后没有pom.xml
<java.version>1.8</java.version>
<tomcat.version>8.5.14</tomcat.version>

2.修改数据源，容器端口，application.properties
datasource信息
server端口号

3.修改后台Logo，/views/index.jsp
39行jspX改为ZW

4.修改后台版本信息，/views/core/homepage/welcome.jsp
164行

5.修改title，/views/title.jsp
1行

6.修改网站图标favicon.ico
直接替换webapp下的favicon.ico

7.修改Enterprise.java为企业版，否则ueditor上会出现"doc导入功能在商业版中提供"
EP = true

8.登录页，/views/login.jsp

=========================================
部署后修改
1.通过角色权限屏蔽没有开放的功能
模块组件/定时任务
模块组件/附件管理
系统管理/发布点
系统管理/工作流组
系统管理/工作流
系统管理/操作日志

2.
网站设置 / 基本设置、水印设置、自定义设置
系统设置 / 基本设置 端口、自定义设置版权

==========================================
合并模板
1.创建站点
创建站点时会自动将当前所在站点下的模板复制到新站点下；
模板所在的文件夹为新建站点的ID，第一个新建站点的ID为15；
2.创建主题模板
在/template/15下新建文件夹changguang，即模板主题；
在/template/15/changguang下新建文件夹_files，用来放置html引用的资源文件；
html中引用资源改为"_files/*/*.js"；
3.模型管理-首页模型
修改首页模型，指定站点首页所使用的模板；
4.访问站点
使用 localhost:8080/site-站点编码 访问站点；
如果要使用域名访问，则必须勾选“域名识别”；
5.模型管理-栏目模型
分析站点划分栏目，然后新建栏目模型，没有栏目模型建不了栏目，新建栏目时必须选择栏目模型，而且栏目模型要选一些系统字段；
分析站点划分文档，然后新建文档模型；

新建栏目，栏目通常就是网站得导航，不想显示的可以在列表上勾选“前台隐藏”；


============================================
问题
1.上传按钮不好使
首先要保证浏览器安装了flash插件，因为swfupload依赖flash实现；
如果是chrome浏览器，打开设置-》隐私设置-内容设置-》flash 勾选允许运行flash-》完成；

